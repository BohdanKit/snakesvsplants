//
// Created by rion on 02.03.21.
//
#ifndef PROJ_ANDROID_SCENEMENU_H
#define PROJ_ANDROID_SCENEMENU_H
#include "cocos2d.h"
//#include "CocosGUI.h"
#include "ProgrammingTools.h"
#include "ui/UIButton.h"


class SceneMenu : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    // implement the "static create()" method manually
    CREATE_FUNC(SceneMenu);
private:
    cocos2d::ui::Button* startButton;
    Sprite* background;
    //bacground
};



#endif //PROJ_ANDROID_SCENEMENU_H
