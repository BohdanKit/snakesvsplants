//
// Created by rion on 02.03.21.
// common properties for all snakes like SPEED and MAXIMUM AVAILABLE GROWTH
//

#ifndef PROJ_ANDROID_COMMONPROPERTIESOFSNAKES_H
#define PROJ_ANDROID_COMMONPROPERTIESOFSNAKES_H

struct Pos
{
    int x;
    int y;
};


struct ContainSquare{
    unsigned snakeID;
    unsigned floverID;
};


struct CommonPropertiesOfSnakes {
    int pxWidth;//the width of the playing field in pixels
    int pxHeight;
    int areaWidth;//the width of the playing field in areas (how many steps in width)
    int areaHeight;
    //массив ходов может сетить сама карта (у нее для этого есть указатель на змею)
    //то ессть - змея дает свой массив ходов в доступ для карты
    //НЕТ змея будет получать ссылку на массив ходов
    //map on snakeID with pointers on time intervals on Pos of steps for every snake
    std::map <unsigned, Pos**> posHeadInTime;//ok
    //ссылка на структуру // на номер змеи // на временной интервал // на позицию
    int maxTimeLayers; //need for determinate size of array of time slots
    int curTimeInterval; //current time slot number // old time slots are removed and therefore cur time indexing in array is reset



};
    // [id][Snake]



    // относительные размеры игрового поля, размеры игрового поля в пикселях(для масштаба),
    // относительная позиция на игровом поле // id // массив ходов // скорость движения (тек&мкс) // скорость роста // начальная длинна & текущая возможная длинна & мак длина
    // индикатор о текущем номере хода, и сколько времени уже прошло с его начала
    // индикатор о том, что нужно расти до максимума
    // индикатор о том, что нужно применить ускорение (это передавать вместе с номером хода)

#endif //PROJ_ANDROID_COMMONPROPERTIESOFSNAKES_H
