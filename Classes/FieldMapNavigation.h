//
// Created by rion on 02.03.21.
// Field Map Navigation
// multi layer map with different object on every layer
// size of field dependence from display aspect ratio and physical size

#ifndef PROJ_ANDROID_FIELDMAPNAVIGATION_H
#define PROJ_ANDROID_FIELDMAPNAVIGATION_H
#include "cocos2d.h"
#include <map>
#include "CommonPropertiesOfSnakes.h"
#include "Snakes.h"

USING_NS_CC;

class Snakes;

enum Direction
{
    UP,
	DOWN,
    LEFT,
    RIGHT
};

class FieldMapNavigation {
    friend class Snake;
    Snakes* snakes = nullptr; //need initiate snakes spawn and tell where they can go
    //FIELD PROPERTIES
    int mapWidth = 0;//number of parts across the width of the field
    int mapHeight = 0;//number of parts across the height of the field
    //snakes parameters
    //
    int maxLength;
    int stepsToLengthUp; //how many steps to increase the length
    int prevTimeInterval = 0; // curTimeInterval - prevTimeInterval = count items of map2DInTime(posHeadInTime) for destroy
    //float timeStepNormalSpeed = 1.0; //maximum duration of one step (sec)
    //float timeGrowth = 2;//growth time per one division in seconds

    //Snake* snakesw;
    //эти все копирующиеся переменные не нужны
    //они будут браться у дружественных змей
    // нужно будет сохранить например это -> float maxSnakeSpeedGrowth = 0.5;//length of growth at step
    int maxTimeLayers = 20;
    int minTimeLayers = 15;
    //int a = 5;

    //snake parameters // no spped
    float curSnakeSpeed = 2;//at sec
    float minSnakeSpeed = 2;//at sec
    float maxSnakeSpeedGrowth = 0.5;//length of growth at step
    int maxSnakeLength = 10;//maximum possible length

    //map data
    ContainSquare**** map2DInTime;//[time][row][column][data field contain(snakeID flowerID)] //lock this array for time update
    bool availableMap2DInTime = false;
    int lastTimeIndexUpdate =  -1;
    int curTimeInterval = 0;
    //data for navigation shakes
    std::map <unsigned, Pos**> posHeadInTime;//coordinates of the snake's head by ID at a specific time // [id][time][data posittion]
    //map <int, int> mp;

    //control map
    bool updateDirections(int curTimeInterval);//execute in a separate thread //reset cur time interval
    bool addNewSnake(int timeIntervalForCreate, unsigned id);
    Pos* getRandPosForSnake();
    Direction getRandDirection(const Pos* posHead, const std::vector<Pos*> tempNavigation);
    int howFastSnakeLeavePos(Pos posCheck, int stepsToPosCheck, unsigned snakeID ); //return max count step to leave cur pos by this snake
    std::vector<Pos*> getNewSnake();//triggered when next step should be new snake

public:
    FieldMapNavigation(Snakes* snakes);
    ~FieldMapNavigation();
    //метод создания игрового поля
    //bool initMapNavigation(Snakes* snakes);
    //метод добавления новой змеи
    //метод проклвдывания маршрута для змеи
    //метод добавления змеи в следующем промежутке времени
    //метод

};


#endif //PROJ_ANDROID_FIELDMAPNAVIGATION_H
