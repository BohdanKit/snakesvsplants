//
// Created by rion on 02.03.21.
//

#ifndef PROJ_ANDROID_PROGRAMMINGTOOLS_H
#define PROJ_ANDROID_PROGRAMMINGTOOLS_H
#include "cocos2d.h"
#include "Snakes.h"

USING_NS_CC;
using namespace std;

namespace ProgrammingTools {
    static Sprite* makeCenterOfSpriteWithSize(const string & filename, Size requiredSize){
        //auto requiredSize = Size(300,300);
        //std::string filename =  "SceneMenuBackground.jpg";

        Sprite* sprite = nullptr;
        Sprite* tSprite = Sprite::create(filename);
        Size carrentSize = tSprite->getContentSize();


        float requiredRatio = requiredSize.width / requiredSize.height;
        float carrentRatio = carrentSize.width / carrentSize.height;
        double scale = 1;
        log("requiredSize.width = %f |requiredSize.height = %f ",requiredSize.width, requiredSize.height );
        log("carrentSize.width = %f |carrentSize.height = %f ",carrentSize.width, carrentSize.height );
        if(requiredRatio > carrentRatio ){// enlarge the picture in width
            scale = requiredSize.width / carrentSize.width;
            int catOfSide = (int)( (carrentSize.height * scale - requiredSize.height) / 2 / scale );
            log("catOfSide = %d |carrentSize.height = %f ",catOfSide, carrentSize.height );
            log("requiredSize.height = %f |scale = %f |requiredSize.height / scale + catOfSide = %f", requiredSize.height,scale ,requiredSize.height / scale + catOfSide );
            //log("catOfSide = %f |carrentSize.height = %f |requiredSize.height = %f |scale = %f |requiredSize.height / scale + catOfSide = %f",catOfSide, carrentSize.height, requiredSize.height,scale ,requiredSize.height / scale + catOfSide );
            sprite = Sprite::create(filename, Rect(0, catOfSide, requiredSize.width / scale, requiredSize.height / scale)); //cut image from top and buttom side
            sprite->setScale(scale);
        }
        else{// enlarge the picture in height
            scale = requiredSize.height / carrentSize.height;
            int catOfSide = (int)( (carrentSize.width * scale - requiredSize.width) / 2 / scale );
            sprite = Sprite::create(filename, Rect(catOfSide, 0, requiredSize.width / scale + catOfSide, requiredSize.height / scale )); //cut image from left and right side
            sprite->setScale(scale);
        }
        //Sprite::create("emoji.png", Rect(0, 0, 27, 25 ))
        //Sprite* sprite = Sprite::create(filename);

        //

        //delete carrentSize;
        //delete tSprite;

        return sprite;
    }

    static void printlog(){
        cocos2d::log("232 testing1234567");
    }
};


#endif //PROJ_ANDROID_PROGRAMMINGTOOLS_H
