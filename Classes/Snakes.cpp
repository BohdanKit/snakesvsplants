//
// Created by rion on 02.03.21.
//

#include "Snakes.h"

Snakes::Snakes()
{
}

Snakes::~Snakes(){
    cocos2d::log("232 DESTRUKT Snakes");
}

bool Snakes::init(Size pxSize)
{
    this->pxSize = pxSize;
    int dpi = cocos2d::Device::getDPI();
    widthOfPieceOfMap = dpi / 10.0;
    mapWidth = pxSize.width / widthOfPieceOfMap;
    mapHeight = pxSize.height / widthOfPieceOfMap;
    log("DEBUG: mapWidth = %d, mapHeight = %d, widthOfPieceOfMap = %f", mapWidth, mapHeight, widthOfPieceOfMap);


    drawPolygone = DrawNode::create();
    this->addChild(drawPolygone, 1);


    //init milty touch listener
    touchListener = EventListenerTouchAllAtOnce::create();
    touchListener->onTouchesBegan = [=](const std::vector<Touch*>& touches, Event* event){
        Vec2 touchPos = touches[0]->getStartLocation();
        log("DEBUG: touchPosttt = %f, %f", touchPos.x, touchPos.y);
        //this->id = 5;
        return true; // check if it Snakes area (not menu area)
    };
    _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

    //initiate navigation for snakes
    mapNavigation = new FieldMapNavigation(this);
    //mapNavigation->initMapNavigation(this); //inicialize variable for neext navigation
    //mapNavigation->start();//run renderring navigation

//    start();//start drawing

    return true;
}

Snakes* Snakes::create(Size pxSize)
{
    Snakes *snakes = new Snakes();
    if ( snakes && snakes->init(pxSize) )
    {
        snakes->autorelease();
        return snakes;
    }
    CC_SAFE_DELETE(snakes);
    return nullptr;
}

void Snakes::draw(Renderer* renderer, const Mat4 & transform_, uint32_t flags)
{
    //на основании времени текущего шага определяется позиция змеи и соответственно ее состояние анимации
    //auto sprite_ = Sprite::create("HelloWorld.png");
        //sprite_->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
        //log("DEBUG: origin 123 = %f, %f", origin.x, origin.y);
        //this->addChild(sprite_, 0);
        //
        //auto sprite = Sprite::create("HelloWorld.png");
        //Sprite* sprite = Sprite::create();
        //sprite->setColor()
        //sprite->setTextureRect( Rect(0, 0, 400 ,400) );
        //sprite->setOpacity(100);
        //DrawNode* drawPolygone = DrawNode::create();
        yt+=1;
        if (yt>=100){yt = 0;}

        Point* Block1 = new Point[12]{
            Point(0+yt,0),
            Point(30+yt,0),
            Point(50+yt,10),
            Point(80+yt,10),
            Point(100+yt,0),
            Point(200+yt,0),
            Point(200+yt,40),
            Point(100+yt,40),
            Point(80+yt,50),
            Point(50+yt,50),
            Point(30+yt,40),
            Point(0+yt,40)
        };

        drawPolygone->clear();
        drawPolygone->drawPolygon(Block1,12 ,Color4F(1,222,120,1), 1, Color4F(0,0,0,1));


        delete[] Block1;
        Block1 = nullptr;

        //sprite->addChild(drawPolygone, 10);
    //змея из блоков, рисуется по блокам
    //или есть только позиция головы и время прошлого движения
    //есть дата хранящая текущее смещение относительно прошлой координаты
    //змея рендерится в зависимости от точек в сетке
    //рендер начинается с головы
    // 1) значит нужно сделать рендер змеи по координатам на сетке и смещением в отношении следующей координаты
    // смещение доступно остальным, чтобы рендерить click area

    //значит нужно сделать рендер змеи с головы по координатам, а потом еще и со смещением
    //сначала чекнуть таймеры
    //будет таймер начала текущего хода для определения текущего смещения
}

void Snakes::update(float delta)//можно склыдывать дельты = время после старта
{
    static float timeForNewSnake = 0;//first snake was created with constructor Snakes::init(Size pxSize)
    timeForNewSnake += delta;
    absolutelyTimeOfCurStep += delta;
    if (absolutelyTimeOfCurStep - timeStepCurSpeed > 1)
    {
        curTimeInterval++;
        absolutelyTimeOfCurStep = absolutelyTimeOfCurStep - timeStepCurSpeed;
    }
    if (timeForNewSnake + timeStepCurSpeed > timeToCreateNewSnake) //check that the next time must be added new snake
    {
        FieldMapNavigation->addNewSnake(curTimeInterval + 1, ++counterOfSnakes);//created new snake if time for create
        timeForNewSnake -= timeToCreateNewSnake; //= (timeForNewSnake + timeStepCurSpeed) - timeToCreateNewSnake - timeStepCurSpeed;
    }



    curTimeInterval = curTimeInterval + 1;

    FieldMapNavigation->mapNavigation.updateDirections(curTimeInterval);//only update for already created snakes

}

