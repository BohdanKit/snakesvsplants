//
// Created by rion on 02.03.21.
//

#include "SceneGame.h"
//#include "SimpleAudioEngine.h"
#include <iostream>

USING_NS_CC;
using namespace ProgrammingTools;
using namespace std;


Scene* SceneGame::createScene()
{
    return SceneGame::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in SceneMenu.cpp\n");
}

// on "init" you need to initialize your instance
bool SceneGame::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    /*
    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
    auto closeItem = MenuItemImage::create(
            "CloseNormal.png",
            "CloseSelected.png",
            CC_CALLBACK_1(SceneGame::menuCloseCallback, this));

    if (closeItem == nullptr ||
        closeItem->getContentSize().width <= 0 ||
        closeItem->getContentSize().height <= 0)
    {
        problemLoading("'CloseNormal.png' and 'CloseSelected.png'");
    }
    else
    {
        float x = origin.x + visibleSize.width - closeItem->getContentSize().width/2;
        float y = origin.y + closeItem->getContentSize().height/2;
        closeItem->setPosition(Vec2(x,y));
    }

    // create menu, it's an autorelease object
    auto menu = Menu::create(closeItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);

*/

    //backgroud
    //Sprite* sprite = Sprite::create("SceneMenuBackground.jpg");
    Sprite* sprite = makeCenterOfSpriteWithSize("gress.jpg", visibleSize);
    if (sprite == nullptr)
    {
        problemLoading("'gress.jpg'");
    }
    else
    {
        sprite->setAnchorPoint(Vec2(0, 0));
        // position the sprite on the center of the screen
        sprite->setPosition(Vec2(origin.x, origin.y));
        // add the sprite as a child to this layer
        this->addChild(sprite, 0);
    }

    //

    Size frameSize = Director::getInstance()->getOpenGLView()->getFrameSize();
    log("frameSize.width = %f |frameSize.height = %f ",frameSize.width, frameSize.height );

    //init PlayArea
    playArea = PlayArea::create();
    playArea->setAnchorPoint(Vec2(0, 0));
    playArea->setPosition(Vec2(origin.x, origin.y));
    this->addChild(playArea, 1);

    //draw snake
    /*
    auto snakes = Snakes::create(nullptr);
    snakes->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    this->addChild(snakes, 2);
    */
    //
    /*
    auto sprite_ = Sprite::create("HelloWorld.png");
    sprite_->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
        log("DEBUG: origin 123 = %f, %f", origin.x, origin.y);
        // add the sprite_ as a child to this layer
        this->addChild(sprite_, 0);
*/
    //auto d = cocos2d::Director::getInstance()->getDPI();
    int d = cocos2d::Device::getDPI();
    //auto d = cocos2d::Director().getWinSize().height;
    log("getDPI = %d ", d );

    return true;
}



void SceneGame::menuCloseCallback(Ref* pSender)
{
    //Close the cocos2d-x game scene and quit the application
    Director::getInstance()->end();

    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() as given above,instead trigger a custom event created in RootViewController.mm as below*/

    //EventCustom customEndEvent("game_scene_close_event");
    //_eventDispatcher->dispatchEvent(&customEndEvent);


}
