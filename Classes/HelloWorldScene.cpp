/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.

 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#include "HelloWorldScene.h"
//#include "SimpleAudioEngine.h"
#include <iostream>
#include <vector>

USING_NS_CC;

Scene* HelloWorld::createScene()
{
    return HelloWorld::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
void myFunc(int y)
{
    log("DEBUG: mySprite 12345");
}

bool HelloWorld::init()
{
    //using namespace std;
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
    auto closeItem = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));

    if (closeItem == nullptr ||
        closeItem->getContentSize().width <= 0 ||
        closeItem->getContentSize().height <= 0)
    {
        problemLoading("'CloseNormal.png' and 'CloseSelected.png'");
    }
    else
    {
        float x = origin.x + visibleSize.width - closeItem->getContentSize().width/2;
        float y = origin.y + closeItem->getContentSize().height/2;
        closeItem->setPosition(Vec2(x,y));
    }

    // create menu, it's an autorelease object
    auto menu = Menu::create(closeItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);

    /////////////////////////////
    // 3. add your codes below...

    // add a label shows "Hello World"
    // create and initialize a label

    auto label = Label::createWithTTF("Hello Dragon 172", "fonts/Marker Felt.ttf", 24);
    if (label == nullptr)
    {
        problemLoading("'fonts/Marker Felt.ttf'");
    }
    else
    {
        // position the label on the center of the screen
        label->setPosition(Vec2(origin.x + visibleSize.width/2,
                                origin.y + visibleSize.height - label->getContentSize().height));

        // add the label as a child to this layer
        this->addChild(label, 1);
    }

    // add "HelloWorld" splash screen"
    auto sprite = Sprite::create("HelloWorld.png");
    if (sprite == nullptr)
    {
        problemLoading("'HelloWorld.png'");
    }
    else
    {
        // position the sprite on the center of the screen
        sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
        log("DEBUG: origin 123 = %f, %f", origin.x, origin.y);
        // add the sprite as a child to this layer
        this->addChild(sprite, 0);
    }

    std::vector<Sprite*> emojiSprites;
    //auto mySprite = Sprite::create("emoji.png", Rect(0,0,40,40));

    for (int i = 0; i < 11; i++){
        log("DEBUG: origin = %f, %f", origin.x, origin.y);
        for (int j = 0; j < 5; j++){
            //emojiSprites.
            emojiSprites.push_back(Sprite::create("emoji.png", Rect(0, 0, 27, 25 )));
            //emojiSprites.push_back(Sprite::create("emoji.png", Rect(1120/11*i, 584/5*j, 1120/11*(i+1), 584/5*(j+1) )));
        }
    }


    //

    ////
    Sprite* mySprite = emojiSprites[0];
    this->addChild(mySprite, 0);


    auto listener1 = EventListenerTouchOneByOne::create();
    // trigger when you push down
    listener1->onTouchBegan = [](Touch* touch, Event* event){
        Vec2 touchPos = touch->getStartLocation();
        log("DEBUG: touchPos = %f, %f", touchPos.x, touchPos.y);
        myFunc(6);
        //log("DEBUG: mySprite = %f, %f", mySprite->getPosition().x, mySprite->getPosition().y);
        //log("DEBUG: mySprite = %f, %f", mySprite->getPosition->x, mySprite->getPosition->y);
        // your code
        return true; // if you are consuming it
    };
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener1, this);
    myFunc(6);
    ////

    //Sprite* mySprite = emojiSprites[0];

    //HelloWorld::Scene::addChild(mySprite, 0);

    auto dir = Director::getInstance();
    mySprite->setPosition(Vec2(dir->getVisibleSize().width / 2, mySprite->getContentSize().height / 2 + origin.y ));

    log("DEBUG: mySprite = %f, %f", mySprite->getPosition().x, mySprite->getPosition().y);
    // создание действия MoveBy в то место, от куда мы сбросим спрайт
    auto move = MoveBy::create(2, Vec2(0,dir->getVisibleSize().height - mySprite->getContentSize().height ));
    auto move_back = move->reverse();

    // создаем действие - падение
    auto move_ease_back = EaseBounceOut::create(move_back->clone());

    // создает задержку, которая запускается между действиями
    auto delay = DelayTime::create(0.25f);

    // создаем последовательность действий в том порядке,
    // в котором мы хотим их запустить
    auto seq1 = Sequence::create( move, delay, move_ease_back, nullptr);

    // создает последовательность и повторяет бесконечно
    mySprite->runAction(RepeatForever::create(seq1));




/*
    int vector_size = emojiSprites.size();
    for (int i = 0; i < vector_size; i++) {


    }

    auto seq = Sequence::create(mySpawn, CallFunc::create(callback), nullptr);

    do{//drawing


    } while(true);
*/



/*
    ///game start
    int SMILES_COUNT = 10;
    for (int i = 0; i < SMILES_COUNT; i++) {
        std::string result = "emoji-" + itos(i);
        Sprite *sprite = Sprite::create(result);

        if (sprite != NULL) {
            this->addChild(sprite);
            listSprites.push_front(sprite);
        }
    }
*/
    return true;
}


void HelloWorld::menuCloseCallback(Ref* pSender)
{
    //Close the cocos2d-x game scene and quit the application
    Director::getInstance()->end();

    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() as given above,instead trigger a custom event created in RootViewController.mm as below*/

    //EventCustom customEndEvent("game_scene_close_event");
    //_eventDispatcher->dispatchEvent(&customEndEvent);


}

